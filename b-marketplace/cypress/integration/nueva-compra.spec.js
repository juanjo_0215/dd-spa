
describe('Registrar una compra', () => {
    it('Ir a la pagina principal', () => {
      cy.visit('http://localhost:4200/');
      cy.get('#nombre').type('Maracuya');
      cy.get('#imagenUrl').type('20');
      cy.get('#guardar').click();

      cy.contains('Se ha elegido a Maracuya');
    });
  });
