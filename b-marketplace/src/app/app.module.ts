import { BrowserModule } from '@angular/platform-browser';
import { NgModule, InjectionToken, Injectable, APP_INITIALIZER } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActionReducerMap, StoreModule as NgRxStoreModule, Store } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { GoogleMapsModule } from '@angular/google-maps';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BusquedaCompraComponent } from './components/busqueda-compra/busqueda-compra.component';
import { ListaComprasComponent } from './components/lista-compras/lista-compras.component';
//import { BusquedaApiClient } from './models/busqueda-api-client.model';
import { BusquedaCompraState, reducerBusquedaCompra, BusquedaCompraEffects, intializeBusquedaCompraState, InitMyDataAction } from './models/busqueda-compra-state.model';
import { DetalleCompraComponent } from './components/detalle-compra/detalle-compra.component';
import { FormDetalleCompraComponent } from './components/form-detalle-compra/form-detalle-compra.component';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { AuthService } from './services/auth.service';
import { PromocionesComponent } from './components/promociones/promociones/promociones.component';
import { PromocionesMainComponent } from './components/promociones/promociones-main/promociones-main.component';
import { PromocionesMasInfoComponent } from './components/promociones/promociones-mas-info/promociones-mas-info.component';
import { PromocionesDetalleComponent } from './components/promociones/promociones-detalle/promociones-detalle.component';
import { ReservasModule } from './reservas/reservas.module';
import { ReservasListadoComponent } from './reservas/reservas-listado/reservas-listado.component';
import { HttpClientModule, HttpHeaders, HttpRequest, HttpClient } from '@angular/common/http';
import Dexie from 'dexie';
import { BusquedaCompra } from './models/busqueda-compra.model';
import {TranslateModule, TranslateLoader, TranslateService} from '@ngx-translate/core';
import { Observable, from } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { EspiameDirective } from './espiame.directive';
import { TrackearClickDirective } from './trackear-click.directive';

// init routing

export const childrenRoutesVuelos: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'main', component: PromocionesMainComponent },
  { path: 'mas-info', component: PromocionesMasInfoComponent },
  { path: ':id', component: PromocionesDetalleComponent },
];

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: ListaComprasComponent },
  { path: 'detalle', component: DetalleCompraComponent },
  { path: 'login', component: LoginComponent },
  {
    path: 'protected',
    component: ProtectedComponent,
    canActivate: [ UsuarioLogueadoGuard ]
  },
  {
    path: 'promociones',
    component: PromocionesComponent,
    canActivate: [ UsuarioLogueadoGuard ],
    children: childrenRoutesVuelos
  },
  {
    path: 'reservas',
    component: ReservasListadoComponent,
    children: childrenRoutesVuelos
  }
];

// end init routing

// app config
export interface AppConfig {
  apiEndpoint: String;
}
const APP_CONFIG_VALUE: AppConfig = {
  apiEndpoint: 'http://localhost:3000'
};
export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');
// fin app config

// redux init

export interface AppState {
  compras: BusquedaCompraState;
}

const reducers: ActionReducerMap<AppState> = {
  compras: reducerBusquedaCompra
};

const reducersInitialState = {
    compras: intializeBusquedaCompraState()
};

// fin redux init

// app init
export function init_app(appLoadService: AppLoadService): () => Promise<any>  {
  return () => appLoadService.intializeBusquedaComprasState();
}

@Injectable()
class AppLoadService {
  constructor(private store: Store<AppState>, private http: HttpClient) { }
  async intializeBusquedaComprasState(): Promise<any> {
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndpoint + '/my', { headers: headers });
    const response: any = await this.http.request(req).toPromise();
    this.store.dispatch(new InitMyDataAction(response.body));
  }
}

// fin app init

// dexie db
export class Translation {
  constructor(public id: number, public lang: string, public key: string, public value: string) {}
}

@Injectable({
  providedIn: 'root'
})
export class MyDatabase extends Dexie {
  compras: Dexie.Table<BusquedaCompra, number>;
  translations: Dexie.Table<Translation, number>;
  constructor () {
      super('MyDatabase');
      this.version(1).stores({
        compras: '++id, nombre, cantidad'
      });
      this.version(2).stores({
        compras: '++id, nombre, cantidad',
        translations: '++id, lang, key, value'
      });
  }
}

export const db = new MyDatabase();
// fin dexie db

// i18n ini
class TranslationLoader implements TranslateLoader {
  constructor(private http: HttpClient) { }

  getTranslation(lang: string): Observable<any> {
    const promise = db.translations
                      .where('lang')
                      .equals(lang)
                      .toArray()
                      .then(results => {
                                        if (results.length === 0) {
                                          return this.http
                                            .get<Translation[]>(APP_CONFIG_VALUE.apiEndpoint + '/api/translation?lang=' + lang)
                                            .toPromise()
                                            .then(apiResults => {
                                              db.translations.bulkAdd(apiResults);
                                              return apiResults;
                                            });
                                        }
                                        return results;
                                      }).then((traducciones) => {
                                        //console.log('traducciones cargadas:');
                                        //console.log(traducciones);
                                        return traducciones;
                                      }).then((traducciones) => {
                                        return traducciones.map((t) => ({ [t.key]: t.value}));
                                      });
    /*
    return from(promise).pipe(
      map((traducciones) => traducciones.map((t) => { [t.key]: t.value}))
    );
    */
   return from(promise).pipe(flatMap((elems) => from(elems)));
  }
}

function HttpLoaderFactory(http: HttpClient) {
  return new TranslationLoader(http);
}

// fin i18n ini


@NgModule({
  declarations: [
    AppComponent,
    BusquedaCompraComponent,
    ListaComprasComponent,
    DetalleCompraComponent,
    FormDetalleCompraComponent,
    LoginComponent,
    ProtectedComponent,
    PromocionesComponent,
    PromocionesMainComponent,
    PromocionesMasInfoComponent,
    PromocionesDetalleComponent,
    EspiameDirective,
    TrackearClickDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    NgRxStoreModule.forRoot(reducers, {
      initialState: reducersInitialState,
      runtimeChecks: {
        strictStateImmutability: false,
        strictActionImmutability: false
      }
    }),
    EffectsModule.forRoot([BusquedaCompraEffects]),
    StoreDevtoolsModule.instrument(),
    ReservasModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: (HttpLoaderFactory),
          deps: [HttpClient]
      }
    }),
    GoogleMapsModule,
    NgxMapboxGLModule,
    BrowserAnimationsModule
  ],
  providers: [
    //BusquedaApiClient,
    AuthService,
    UsuarioLogueadoGuard,
    { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
    AppLoadService,
    { provide: APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi: true },
    MyDatabase
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
