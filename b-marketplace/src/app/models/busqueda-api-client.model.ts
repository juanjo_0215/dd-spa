import { BusquedaCompra } from './busqueda-compra.model';
import { Subject, BehaviorSubject } from 'rxjs';
import { Injectable, forwardRef, Inject } from '@angular/core';
import { HttpRequest, HttpHeaders, HttpClient, HttpEvent, HttpResponse } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { AppState, APP_CONFIG, AppConfig, db } from '../app.module';
import { NuevoCompraAction } from './busqueda-compra-state.model';

@Injectable()
export class BusquedaApiClient {
	
	compras: BusquedaCompra[];
	current: Subject<BusquedaCompra> = new BehaviorSubject<BusquedaCompra>(null);
	public nombre:string;
	public cantidad:string;
	
	constructor(
		private store: Store<AppState>,
		@Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
		private http: HttpClient
	  ) {
		this.store
		  .select(state => state.compras)
		  .subscribe((data) => {
			//console.log('destinos sub store');
			//console.log(data);
			this.compras = data.items;
		  });
		this.store
		  .subscribe((data) => {
			//console.log('all store');
			//console.log(data);
		  });
	  }

	getById(id: String): BusquedaCompra {
		return this.compras.filter(function(d) { return d.id.toString() === id; })[0];
	}
	
	/* ANTERIOR
	add(d: BusquedaCompra){
		this.compras.push(d);
	}*/

	add(d: BusquedaCompra) {
		const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
		const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', { nuevo: d.nombre }, { headers: headers });
		this.http.request(req).subscribe((data: HttpResponse<{}>) => {
		  if (data.status === 200) {
			this.store.dispatch(new NuevoCompraAction(d));
			const myDb = db;
			myDb.compras.add(d);
			//console.log('todos los destinos de la db!');
			myDb.compras.toArray().then(compras => console.log(compras));
		  }
		});
	  }
	
	getAll(): BusquedaCompra[]{
		return this.compras;
	}

	elegir (c: BusquedaCompra) {
		this.compras.forEach(x => x.setSelected(false));
		c.setSelected(true);
		this.current.next(c);
	}

	subscribeOnChange(fn) {
		this.current.subscribe(fn);
	}

}