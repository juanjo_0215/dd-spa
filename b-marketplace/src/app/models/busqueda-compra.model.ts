import { TrackearClickDirective } from '../trackear-click.directive';

export class BusquedaCompra {

	private selected: boolean;
	public servicios: string[];
	public votes: number = 0;
	id: any;
	
	constructor(public nombre:string, public cantidad:string) { 
		this.servicios = ['Descuentos', 'Bonos'];
	}
	
	isSelected(): boolean{
		return this.selected;
	}
	
	setSelected(s: boolean) {
		this.selected=s;
	}

	voteUp() {
		this.votes++;
	}

	voteDown() {
		this.votes--;
	}

	voteReset() {
		this.votes = 0;
	}

}