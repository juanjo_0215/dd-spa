import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { BusquedaCompra } from './busqueda-compra.model';
import { BusquedaApiClient } from './busqueda-api-client.model';
import { HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';

// ESTADO
export interface BusquedaCompraState {
    items: BusquedaCompra[];
    loading: boolean;
    favorito: BusquedaCompra;
}

export function intializeBusquedaCompraState() {
  return {
    items: [],
    loading: false,
    favorito: null
  };
}

// ACCIONES
export enum BusquedaCompraActionTypes {
  NUEVO_DESTINO = '[Busqueda Compra] Nuevo',
  ELEGIDO_FAVORITO = '[Busqueda Compra] Favorito',
  VOTE_UP = '[Busqueda Compra] Vote Up',
  VOTE_DOWN = '[Busqueda Compra] Vote Down',
  RESET_ITEM = '[Busqueda Compra] Reset Item',
  INIT_MY_DATA = '[Busqueda Compra] Init My Data',
  TRACKING_TAGS = '[Busqueda Compra]Traching Tags'
}

export class NuevoCompraAction implements Action {
  type = BusquedaCompraActionTypes.NUEVO_DESTINO;
  constructor(public compra: BusquedaCompra) {}
}

export class ElegidoFavoritoAction implements Action {
  type = BusquedaCompraActionTypes.ELEGIDO_FAVORITO;
  constructor(public compra: BusquedaCompra) {}
}

export class VoteUpAction implements Action {
  type = BusquedaCompraActionTypes.VOTE_UP;
  constructor(public compra: BusquedaCompra) {}
}

export class VoteDownAction implements Action {
  type = BusquedaCompraActionTypes.VOTE_DOWN;
  constructor(public compra: BusquedaCompra) {}
}

export class VoteResetAction implements Action {
  type = BusquedaCompraActionTypes.RESET_ITEM;
  constructor(public compra: BusquedaCompra) {}
}

export class InitMyDataAction implements Action {
  type = BusquedaCompraActionTypes.INIT_MY_DATA;
  constructor(public compra: string[]) {}
}

export class TrackingTagsAction implements Action {
  type = BusquedaCompraActionTypes.TRACKING_TAGS;
  constructor(public compra: BusquedaCompra) {}
}

export type BusquedaCompraActions = NuevoCompraAction | ElegidoFavoritoAction
  | VoteUpAction | VoteDownAction | VoteResetAction | InitMyDataAction | TrackingTagsAction;

// REDUCERS
export function reducerBusquedaCompra(
  state: BusquedaCompraState,
  action: BusquedaCompraActions
): BusquedaCompraState {
  switch (action.type) {
    case BusquedaCompraActionTypes.INIT_MY_DATA: {
      const destinos: string[] = (action as InitMyDataAction).compra;
      return {
          ...state,
          items: destinos.map((d) => new BusquedaCompra(d, ''))
        };
    }
    case BusquedaCompraActionTypes.NUEVO_DESTINO: {
      return {
          ...state,
          items: [...state.items, (action as NuevoCompraAction).compra ]
        };
    }
    case BusquedaCompraActionTypes.ELEGIDO_FAVORITO: {
      
      state.items.forEach(x => x.setSelected(false));
        const fav: BusquedaCompra = (action as ElegidoFavoritoAction).compra;
        fav.setSelected(true);
        return {
          ...state,
          favorito: fav
        };
    }
    case BusquedaCompraActionTypes.VOTE_UP: {
        const d: BusquedaCompra = (action as VoteUpAction).compra;
        d.voteUp();
        return { ...state };
    }
    case BusquedaCompraActionTypes.VOTE_DOWN: {
        const d: BusquedaCompra = (action as VoteDownAction).compra;
        d.voteDown();
        return { ...state };
    }
    case BusquedaCompraActionTypes.RESET_ITEM: {
      const d: BusquedaCompra = (action as VoteResetAction).compra;
      d.voteReset();
      return { ...state };
    }
    case BusquedaCompraActionTypes.INIT_MY_DATA: {
      const destinos: string[] = (action as InitMyDataAction).compra;
      return {
          ...state,
          items: destinos.map((d) => new BusquedaCompra(d, ''))
        };
    }
    case BusquedaCompraActionTypes.TRACKING_TAGS: {
      console.log("Registro un tag");
      return { ...state };
    }
  }
  return state;
}

// EFFECTS
@Injectable()
export class BusquedaCompraEffects {
  @Effect()
  nuevoAgregado$: Observable<Action> = this.actions$.pipe(
    ofType(BusquedaCompraActionTypes.NUEVO_DESTINO),
    map((action: NuevoCompraAction) => new ElegidoFavoritoAction(action.compra))
  );
  constructor(private actions$: Actions) {}
}
