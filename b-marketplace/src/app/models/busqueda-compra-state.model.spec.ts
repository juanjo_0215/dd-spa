import {
  reducerBusquedaCompra,
  BusquedaCompraState,
  intializeBusquedaCompraState,
  InitMyDataAction,
  NuevoCompraAction
} from './busqueda-compra-state.model';
import { BusquedaCompra } from './busqueda-compra.model';

describe('reducerBusquedaCompra', () => {
  it('should reduce init data', () => {
    //  setup
    const prevState: BusquedaCompraState = intializeBusquedaCompraState();
    const action: InitMyDataAction = new InitMyDataAction(['compra 1', 'compra 2']);
    // action
    const newState: BusquedaCompraState = reducerBusquedaCompra(prevState, action);
    // assertions
    expect(newState.items.length).toEqual(2);
    expect(newState.items[0].nombre).toEqual('compra 1');
  });

  it('should reduce new item added', () => {
    const prevState: BusquedaCompraState = intializeBusquedaCompraState();
    const action: NuevoCompraAction = new NuevoCompraAction(new BusquedaCompra('Arroz', 'url'));
    const newState: BusquedaCompraState = reducerBusquedaCompra(prevState, action);
    expect(newState.items.length).toEqual(1);
    expect(newState.items[0].nombre).toEqual('Arroz');
  });
});
