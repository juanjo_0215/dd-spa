import { Component, OnInit, Output, EventEmitter, Inject, forwardRef } from '@angular/core';
import { BusquedaCompra } from './../../models/busqueda-compra.model';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import { AppConfig, APP_CONFIG } from 'src/app/app.module';

@Component({
  selector: 'app-form-detalle-compra',
  templateUrl: './form-detalle-compra.component.html',
  styleUrls: ['./form-detalle-compra.component.sass']
})
export class FormDetalleCompraComponent implements OnInit {

  @Output() onItemAdded: EventEmitter<BusquedaCompra>;
  fg: FormGroup;
  minLongitud = 2;
  searchResults: string [];

  constructor(formulario: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) { 
    this.onItemAdded = new EventEmitter();
    this.fg = formulario.group({
      nombre: ['', Validators.compose([
        Validators.required,
        this.nombreValidator,
        this.nombreValidatorParametrizable(this.minLongitud)
      ])],
      cantidad: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    const elemNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemNombre, 'input')
    .pipe(
      map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
      filter(text => text.length >= 2),
      debounceTime(120),
      distinctUntilChanged(),
      switchMap((text: string) => ajax(this.config.apiEndpoint + '/productos?q=' + text))
    ).subscribe(ajaxResponse => this.searchResults = ajaxResponse.response);
  }

  guardar(nombre: string, cantidad: string): boolean{
    let c = new BusquedaCompra(nombre, cantidad);
    this.onItemAdded.emit(c);
    return false;
  }


  nombreValidator(control: FormControl): {[s: string]: boolean}{
    let longitud = control.value.toString().trim().length;
    if (longitud >= 15){
      return {invalidNombre: true};
    }
    return null;
  }

  nombreValidatorParametrizable (minLon: number): ValidatorFn{
    return (control: FormControl): {[s: string]: boolean} | null => {
      let longitud = control.value.toString().trim().length;
      if (longitud >= 0 && longitud < this.minLongitud){
        return {minLongNombre: true};
      }
      return null;
    }
  }

}
