import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { BusquedaCompra } from './../../models/busqueda-compra.model';
import { AppState } from './../../app.module';
import { Store } from '@ngrx/store';
import { VoteUpAction, VoteDownAction, VoteResetAction } from './../../models/busqueda-compra-state.model';
import { trigger, state, style, transition, animate } from '@angular/animations';


@Component({
  selector: 'app-busqueda-compra',
  templateUrl: './busqueda-compra.component.html',
  styleUrls: ['./busqueda-compra.component.sass'],
  animations: [
    trigger('esFavorito', [
      state('estadoFavorito', style({
        backgroundColor: 'PaleTurquoise'
      })),
      state('estadoNoFavorito', style({
        backgroundColor: 'WhiteSmoke'
      })),
      transition('estadoNoFavorito => estadoFavorito', [
        animate('3s')
      ]),
      transition('estadoFavorito => estadoNoFavorito', [
        animate('1s')
      ]),
    ])
  ]
})
export class BusquedaCompraComponent implements OnInit {

	@Input () compras: BusquedaCompra;
	@Input () position: number;
	@HostBinding ('attr.class') cssClass='col-md-4';
	@Output() clicked: EventEmitter<BusquedaCompra>;
	@Output() onItemAdded: EventEmitter<BusquedaCompra>;

  	constructor(private store: Store<AppState>) { 
		  this.clicked = new EventEmitter();
		  this.position = 0;
	  }

  	ngOnInit(): void {
  	}

	favorito (){
		this.clicked.emit(this.compras);
		return false;
	}

	voteUp(){
		this.store.dispatch(new VoteUpAction(this.compras));
		return false;
	}

	voteDown(){
		this.store.dispatch(new VoteDownAction(this.compras));
		return false;
	}

	resetVotes() {
		this.store.dispatch(new VoteResetAction(this.compras));
		return false;
	}

}
