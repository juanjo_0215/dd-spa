import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-promociones-detalle',
  templateUrl: './promociones-detalle.component.html',
  styleUrls: ['./promociones-detalle.component.sass']
})
export class PromocionesDetalleComponent implements OnInit {

  id: any;

  constructor(private route: ActivatedRoute) {
    route.params.subscribe(params => { this.id = params['id']; });
  }

  ngOnInit(): void {
  }

}
