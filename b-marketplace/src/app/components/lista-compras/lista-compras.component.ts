import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { BusquedaCompra } from './../../models/busqueda-compra.model';
import { BusquedaApiClient } from './../../models/busqueda-api-client.model';
import { Store } from '@ngrx/store';
import { AppState } from './../../app.module';
import { ElegidoFavoritoAction, NuevoCompraAction, TrackingTagsAction } from './../../models/busqueda-compra-state.model';
import { TrackearClickDirective } from 'src/app/trackear-click.directive';

@Component({
  selector: 'app-lista-compras',
  templateUrl: './lista-compras.component.html',
  styleUrls: ['./lista-compras.component.sass'],
  providers: [BusquedaApiClient]
})
export class ListaComprasComponent implements OnInit {

	@Input () compras: BusquedaCompra;
	@Output() onItemAdded: EventEmitter<BusquedaCompra>;
	updates: string[];

  	constructor(public detallesApiClient: BusquedaApiClient, private store: Store<AppState>) {
		this.onItemAdded = new EventEmitter();
		this.updates = [];
		this.store.select(state => state.compras.favorito)
			.subscribe(data => {
				const fav = data;
				if (data != null){
					this.updates.push("Se ha elegido a " + data.nombre);
				}
			});

			
  	}

  	ngOnInit(): void {
	}

	agregado(compras: BusquedaCompra) {
		this.detallesApiClient.add(compras);
		this.onItemAdded.emit(compras);
		//this.store.dispatch(new NuevoCompraAction(compras));
	}

	elegido (compras: BusquedaCompra){
		this.detallesApiClient.elegir(compras);
		this.store.dispatch(new ElegidoFavoritoAction(compras));
	}

}
