import { Component, OnInit, InjectionToken, Inject, Injectable } from '@angular/core';
import { BusquedaCompra } from 'src/app/models/busqueda-compra.model';
import { BusquedaApiClient } from 'src/app/models/busqueda-api-client.model';
import { ActivatedRoute } from '@angular/router';
import * as Mapboxgl from 'mapbox-gl';

@Component({
  selector: 'app-detalle-compra',
  templateUrl: './detalle-compra.component.html',
  styleUrls: ['./detalle-compra.component.sass'],
  providers: [ BusquedaApiClient ],
  styles: [`
    mgl-map {
      height: 100vh;
      width: 100vw;
    }
  `]
})
export class DetalleCompraComponent implements OnInit {

  compra: BusquedaCompra;

  style = {
    sources: {
      world: {
        type: 'geojson',
        data: 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json'
      }
    },
    version: 8,
    layers: [{
      'id': 'countries',
      'type': 'fill',
      'source': 'world',
      'layout': {},
      'paint': {
        'fill-color': '#6F788A'
      }
    }]
  };

  /*title="gmaps";
  position = {
    lat: -34.681,
    lng: -58.371
  };
  label = {
    color: 'blue',
    text: 'Hola Mundo'
  };*/

  constructor(private route: ActivatedRoute, private busquedaApiClient: BusquedaApiClient) { }

  ngOnInit() {
    
    let id = this.route.snapshot.paramMap.get('nombre');
    //this.compra = this.busquedaApiClient.getById(id);
    
  }

}
