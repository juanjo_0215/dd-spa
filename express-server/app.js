var express = require("express"), cors = require('cors');
var app = express();
app.use(express.json());
app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.listen(3000, () => console.log("Server running on port 3000"));

var productos = [ "Arroz", "Azucar", "Sal", "Frijoles", "Leche", "Aceite", "Mantequilla" ];
app.get("/productos", (req, res, next) => res.json(productos.filter((c)=> c.toLowerCase().indexOf(req.query.q.toString().toLowerCase()) > -1)));

/*ANTERIOR -> app.get("/productos", (req, res, next) => {
  res.json(productos);
});*/

var misCompras = [];
app.get("/my", (req, res, next) => res.json(misCompras));
app.post("/my", (req, res, next) => {
  //console.log(req.body);
  misCompras.push(req.body.nuevo);
  res.json(misCompras);
});
app.get("/api/translation", (req, res, next) => res.json([
  {lang: req.query.lang, key: 'HOLA', value: 'HOLA ' + req.query.lang}
]));
